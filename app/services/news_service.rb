module Services
	class NewsService
		TOKEN = 'ff3bf280d70141b7a8331cf6382d680e'
		URL = "https://newsapi.org/v2/everything?from=2023-12-19&sortBy=publishedAt"

		def initializer(params)
			@query = params[:query]
			@options = { query: @query, apiKey: TOKEN }
		end

		def self.get_news
			@response = HTTParty.get(URL, @options)

			return [] unless @response.success?
		end
	end
end