namespace :db do
	desc 'find articles'
	task populate_articles: :environment do
		unless Article.exists?
			articles_data = new_articles
			articles_data.first(20).each do |article_data|
				Article.create(
					source: article_data["source"]["name"],
					author: article_data["author"],
					title: article_data["title"],
					description: article_data["description"],
					url: article_data["url"],
					urlToImage: article_data["urlToImage"],
					publishedAt: article_data["publishedAt"],
					content: article_data["content"]
				)
			end
			puts 'Articles created'
		else
			puts 'Articles table is not empty.'
		end
	end

	def new_articles
		@response = HTTParty.get(url, { query: {q: 'tesla', apiKey: token, from: today } })

		return [] unless @response.success?

		@response.parsed_response["articles"]
	end

	def url
		'https://newsapi.org/v2/everything'
	end

	def token
		'ff3bf280d70141b7a8331cf6382d680e'
	end

	def today
		'2023-12-19'
	end
end